<?php

use yii\db\Schema;
use yii\db\Migration;

class m141015_112446_edit_blog_image_table extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('{{%blog_image}}', 'title');
        $this->dropColumn('{{%blog_image}}', 'ordering');
    }

    public function safeDown()
    {
        echo "m141015_112446_edit_blog_image_table cannot be reverted.\n";

        return false;
    }
}
