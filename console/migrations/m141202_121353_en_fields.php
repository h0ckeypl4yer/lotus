<?php

use yii\db\Schema;
use yii\db\Migration;

class m141202_121353_en_fields extends Migration
{
    public function up()
    {
        $this->addColumn('{{review}}', 'en_author', Schema::TYPE_STRING . '(50) NOT NULL');
        $this->addColumn('{{review}}', 'en_text', Schema::TYPE_TEXT . ' NOT NULL');

        $this->addColumn('{{portfolio}}', 'en_title', Schema::TYPE_STRING . '(100) NOT NULL');
        $this->addColumn('{{portfolio}}', 'en_text', Schema::TYPE_TEXT . ' NOT NULL');

        $this->addColumn('{{blog_category}}', 'en_title', Schema::TYPE_STRING . '(30) NOT NULL');

        $this->addColumn('{{blog}}', 'en_title', Schema::TYPE_STRING . '(100) NOT NULL');
        $this->addColumn('{{blog}}', 'en_text_first', Schema::TYPE_TEXT . ' NOT NULL');
        $this->addColumn('{{blog}}', 'en_text_second', Schema::TYPE_TEXT . ' NOT NULL');
        $this->addColumn('{{blog}}', 'en_intro', Schema::TYPE_STRING . '(255) NOT NULL');
    }

    public function down()
    {
        echo "m141202_121353_en_fields cannot be reverted.\n";

        return false;
    }
}
