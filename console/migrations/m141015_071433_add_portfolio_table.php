<?php

use yii\db\Schema;
use yii\db\Migration;

class m141015_071433_add_portfolio_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%portfolio}}', [
            'id'    => Schema::TYPE_PK,
            'title' => Schema::TYPE_STRING . '(100) NOT NULL',
            'text'  => Schema::TYPE_TEXT . ' NOT NULL',
            'image' => Schema::TYPE_STRING . ' NOT NULL',
            'ordering' => Schema::TYPE_INTEGER . ' NOT NULL',
            'is_active' => Schema::TYPE_BOOLEAN . ' DEFAULT 1',
            'link' => Schema::TYPE_STRING,
            'type' => Schema::TYPE_INTEGER . ' NOT NULL',
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%portfolio}}');
    }
}
