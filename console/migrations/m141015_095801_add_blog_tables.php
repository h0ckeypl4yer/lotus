<?php

use yii\db\Schema;
use yii\db\Migration;

class m141015_095801_add_blog_tables extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%blog_category}}', [
            'id'       => Schema::TYPE_PK,
            'title'    => Schema::TYPE_STRING . '(30) NOT NULL',
            'image'    => Schema::TYPE_STRING . ' NOT NULL',
            'ordering' => Schema::TYPE_INTEGER . ' NOT NULL'
        ]);

        $this->createTable('{{%blog}}', [
            'id'          => Schema::TYPE_PK,
            'title'       => Schema::TYPE_STRING . '(30) NOT NULL',
            'text_first'  => Schema::TYPE_TEXT . ' NOT NULL',
            'text_second' => Schema::TYPE_TEXT . ' NOT NULL',
            'intro'       => Schema::TYPE_STRING . ' NOT NULL',
            'created_at'  => Schema::TYPE_INTEGER . ' NOT NULL',
            'image'       => Schema::TYPE_STRING . ' NOT NULL',
            'ordering'    => Schema::TYPE_INTEGER . ' NOT NULL',
            'type'        => Schema::TYPE_INTEGER . ' NOT NULL',
            'category_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'video'       => Schema::TYPE_TEXT,
        ]);

        $this->createTable('{{%blog_image}}', [
            'id'       => Schema::TYPE_PK,
            'title'    => Schema::TYPE_STRING . '(30) NOT NULL',
            'image'    => Schema::TYPE_STRING . ' NOT NULL',
            'ordering' => Schema::TYPE_INTEGER . ' NOT NULL',
            'blog_id'  => Schema::TYPE_INTEGER . ' NOT NULL',
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%blog_category}}');
        $this->dropTable('{{%blog}}');
        $this->dropTable('{{%blog_image}}');
    }
}
