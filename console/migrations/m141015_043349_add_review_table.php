<?php

use yii\db\Schema;
use yii\db\Migration;

class m141015_043349_add_review_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%review}}', [
            'id'    => Schema::TYPE_PK,
            'author' => Schema::TYPE_STRING . '(50) NOT NULL',
            'text'  => Schema::TYPE_TEXT . ' NOT NULL',
            'image' => Schema::TYPE_STRING . ' NOT NULL',
            'ordering' => Schema::TYPE_INTEGER . ' NOT NULL',
            'is_active' => Schema::TYPE_BOOLEAN . ' DEFAULT 1',
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%review}}');
    }
}
