<?php

use yii\db\Schema;
use yii\db\Migration;

class m141014_145342_add_slider_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%slider}}', [
            'id'    => Schema::TYPE_PK,
            'title' => Schema::TYPE_STRING . '(32) NOT NULL',
            'link'  => Schema::TYPE_STRING,
            'image' => Schema::TYPE_STRING . ' NOT NULL',
            'ordering' => Schema::TYPE_INTEGER . ' NOT NULL',
            'is_active' => Schema::TYPE_BOOLEAN . ' DEFAULT 1',
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%slider}}');
    }
}
