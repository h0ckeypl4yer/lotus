<?php

use yii\db\Schema;
use yii\db\Migration;

class m141015_074224_add_portfolio_images_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%portfolio_image}}', [
            'id'    => Schema::TYPE_PK,
            'image' => Schema::TYPE_STRING . ' NOT NULL',
            'portfolio_id' => Schema::TYPE_INTEGER . ' NOT NULL',
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%portfolio_image}}');
    }
}
