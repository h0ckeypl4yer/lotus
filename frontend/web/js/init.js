$(function () {

    //$(".header-menu").sticky({topSpacing: 150});


    /*$('.header-menu').on('sticky-start', function () {
        $(this).addClass('header-sticky')
    });
    $('.header-menu').on('sticky-end', function () {
        $(this).removeClass('header-sticky');
    });*/

    var headerTop = $('.header-menu-wrap').offset().top;
    stickHeader();

    $(document).on('scroll', stickHeader);

    function stickHeader(){
        console.log('123');
        if( $(document).scrollTop() > headerTop){
            $('.header-menu-wrap').addClass('header-fixed');
        } else {
            $('.header-menu-wrap').removeClass('header-fixed');
        }
    }

    $('.fancybox').fancybox({
        type: "ajax",
        width: '1032px'
    });

    var mySwiper = new Swiper('.swiper-container', {
        //Your options here:
        mode: 'horizontal',
        loop: false,
        slidesPerView: 'auto',
        scrollContainer: true,
        cssWidthAndHeight: false,
        freeMode: true,
        freeModeFluid: true,
        onlyExternal: true,
        scrollbar: {
            container: '.swiper-scrollbar-in',
            hide: false,
            draggable: true,
        }
        //etc..
    });

    $('.swiper-navi-left').on('click', function (e) {
        e.preventDefault();
        mySwiper.swipePrev();
    });

    $('.swiper-navi-right').on('click', function (e) {
        e.preventDefault();
        mySwiper.swipeNext();
    });

    $('.blog-tabs .blog-tabs-header a').click(function (e) {
        e.preventDefault();
        $('.blog-tabs .blog-tabs-header a').removeClass('active');
        $('.blog-tabs .blog-tabs-item').removeClass('active');
        $($(this).attr('href')).addClass('active');
        $(this).addClass('active');
    });

    $('.blog-list').masonry({
        itemSelector: '.blog-list-item',
        "columnWidth": 260,
        singleMode: false,
    });

    $('input, textarea').placeholder({
        color: '#454545'
    });

    $('body').stickTheFooter();

    $(window).resize(resizeSilder);

    resizeSilder();

    if ($('.blog-item-slider').length) {
        $('.blog-item-slider').bxSlider({
            mode: 'fade',
        });
    }

    $('.reviews-block-slider').bxSlider({
        pager: false,
    });

    var slider = $('.home-slider').bxSlider({
        controls: true,
        pager: false,
        speed: 500,
        pause: 5000,
        mode: 'fade',
        auto: true
    });

    if ($('.parallax-block').length) {
        $(window).scroll(function () {
            var height = $(window).scrollTop() + $(window).height();
            var block_top = $('.parallax-block').offset().top;
            var block_height = 700;

            if (height >= block_top) {
                var offset = (block_top - height) / 2;
                if (offset + block_height <= 0)
                    offset = -block_height;
                $('.parallax-block-in').stop(true, true).animate({'top': offset / 2}, 500)
            }
        });
    }

    if ($('.we-bfly').length) {
        $(window).scroll(function () {
            var imagePos = $('.we-bfly').offset().top;
            var topOfWindow = $(window).scrollTop();

            if (imagePos < topOfWindow + 800) {
                $('.we-bfly').animate({left: 747, top: 219, opacity: 1}, 1500);
            }
        });
    }


});


function resizeSilder() {

    var headerHeight = 0;
    var viewportHeight = $(window).height() - headerHeight;
    var viewportWidth = $(window).width() > 1280 ? $(window).width() : 1280;

    $('.home-slider').css('height', viewportHeight);

    $('.home-slider .home-slider-item').each(function () {
        var element = $(this).find('.home-slider-media-element');
        var itemWidth = element.data('width');
        var itemHeight = element.data('height');

        if (itemWidth / itemHeight > viewportWidth / viewportHeight) {

            var nWidth = itemWidth * viewportHeight / itemHeight;

            var nLeft = (nWidth - viewportWidth) / 2;
            var nRight = nLeft + viewportWidth;

            $(this).css({
                clip: 'rect(0px ' + nRight + 'px ' + viewportHeight + 'px ' + nLeft + 'px)',
                height: viewportHeight + 'px',
                left: -nLeft + 'px',
                top: '0px',
                width: nWidth + 'px',
            });
        } else {
            var nHeight = itemHeight * viewportWidth / itemWidth;
            var nTop = (nHeight - viewportHeight) / 2;
            var nBottom = -nTop + nHeight;
            $(this).css({
                clip: 'rect(' + nTop + 'px ' + viewportWidth + 'px ' + nBottom + 'px 0px)',
                height: nHeight + 'px',
                top: -nTop + 'px',
                left: '0px',
                width: viewportWidth + 'px',
            });
        }
    });

}