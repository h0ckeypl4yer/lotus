<?php

namespace frontend\controllers;

use common\models\Blog;
use common\models\BlogCategory;

class BlogController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $categories = BlogCategory::find()->joinWith('items')->orderBy('ordering, id')->all();
        return $this->render('index', ['categories' => $categories]);
    }

    public function actionView($id)
    {
        return $this->render('view', ['model' => Blog::find()->where('blog.id = :id', [':id' => $id])->joinWith(['addImages', 'category'])->one()]);
    }

}
