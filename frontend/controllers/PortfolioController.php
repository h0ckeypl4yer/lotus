<?php

namespace frontend\controllers;

use common\models\Portfolio;

class PortfolioController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $items = Portfolio::find()->where('is_active = 1')->orderBy('ordering, id')->all();
        return $this->render('index', ['items' => $items]);
    }

    public function actionView($id)
    {
        $this->layout = false;
        $model = Portfolio::find()->joinWith(['addImages'])->where('portfolio.id = :id', [':id' => $id])->one();

        return $this->render('view', ['model' => $model]);
    }

}
