<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'js/libs/fancybox/jquery.fancybox.css',
        'css/styles.css',
    ];
    public $js = [
        'js/libs/jquery-1.10.2.min.js',
        'js/libs/jquery.easing.1.3.js',
        'js/libs/jquery.placeholder.js',
        'js/libs/bxslider.js',
        'js/libs/masonry.pkgd.min.js',
        'js/libs/idangerous.swiper-2.1.min.js',
        'js/libs/idangerous.swiper.scrollbar.js',
        'js/libs/fancybox/jquery.fancybox.pack.js',
        'js/libs/jquery.sticky.js',

        'js/utils.js',
        'js/init.js',
    ];
    public $depends = [
    ];
}
