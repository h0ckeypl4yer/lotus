<?php
/* @var $reviews common\models\Review[] */
?>
<div class="container">
    <div class="reviews-block">
        <div class="reviews-block-slider">

            <?php foreach($reviews as $review) { ?>
                <div class="reviews-item">
                    <div class="reviews-item-image" style="background-image: url(<?= $review->getImageUrl();?>)"></div>
                    <div class="reviews-item-text"><?= $review->{Yii::$app->params['file_prefix'][Yii::$app->session->get('lang', 'ru')] . 'text'}; ?>
                        <div class="reviews-item-author"><?= $review->{Yii::$app->params['file_prefix'][Yii::$app->session->get('lang', 'ru')] . 'author'}; ?></div>
                    </div>
                </div>
            <?php } ?>

        </div>
    </div>
</div>