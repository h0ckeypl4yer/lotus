<?php
namespace frontend\widgets;

use yii;
use yii\base\Widget;
use yii\helpers\Html;

class LangSwitcher extends Widget
{
    public function run()
    {
        $currentUrl = ltrim(Yii::$app->request->url, '/');
        $links = array();
        $links[] = Html::a('RU', '/' . $currentUrl, ['class' => 'header-lang' . (Yii::$app->session->get('lang', 'ru') == 'ru' ? ' active' : '')]);
        $links[] = Html::a('EN', '/en/' . $currentUrl, ['class' => 'header-lang' . (Yii::$app->session->get('lang', 'ru') == 'en' ? ' active' : '')]);
        echo implode(" | ", $links);
    }
}