<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
namespace frontend\widgets;


use common\models\Review;

class Reviews extends \yii\bootstrap\Widget
{
    public function run()
    {
        return $this->render('reviews', ['reviews' => Review::find()->where('is_active = 1')->orderBy('ordering, id')->all()]);
    }
}
