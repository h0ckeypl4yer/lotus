<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
namespace frontend\widgets;

use yii\helpers\Html;
use yii\widgets\Menu;

class MainMenu extends \yii\bootstrap\Widget
{
    public function run()
    {
        $items_left = [
            ['label' => \Yii::t('app', 'Мы'), 'url' => ['/site/we']],
            ['label' => \Yii::t('app', 'Услуги'), 'url' => ['/site/services']],
            ['label' => \Yii::t('app', 'Портфолио'), 'url' => ['/portfolio/index']],
        ];
        $items_right = [
            ['label' => \Yii::t('app', 'Блог'), 'url' => ['/blog/index']],
            ['label' => \Yii::t('app', 'Партнеры'), 'url' => ['#']],
            ['label' => \Yii::t('app', 'Контакты'), 'url' => ['/site/contact']],
        ];
        return Html::beginTag('div', ['class' => 'container'])
            .Html::a('logo', '/', ['class' => 'logo-mini-in'])
        . Html::beginTag('div', ['class' => 'header-menu-left'])
        . Menu::widget(array('items' => $items_left))
        . Html::endTag('div')
        . Html::beginTag('div', ['class' => 'header-menu-right'])
        . Menu::widget(array('items' => $items_right))
        . Html::endTag('div')
        . Html::endTag('div');
    }
}
