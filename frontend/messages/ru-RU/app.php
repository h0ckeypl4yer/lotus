<?php
return [
    'Мы'               => 'We',
    'Услуги'           => 'Services',
    'Портфолио'        => 'Portfolio',
    'Блог'             => 'Blog',
    'Партнеры'         => 'Partners',
    'Контакты'         => 'Contacts',
    'Главная страница' => 'Homepage',
    'Вы можете позвонить в удобное для вас время, написать письмо или задать вопрос через форму обратной связи — я вам обязательно отвечу' => 'You can call at a convenient time for you to write a letter or ask a question via the feedback form - I\'ll be sure to answer',
];