<?php
return [
    'adminEmail' => 'admin@example.com',
    'prefix' => [
        'ru' => '',
        'en' => '/en',
    ],
    'file_prefix' => [
        'ru' => '',
        'en' => 'en_',
    ]
];
