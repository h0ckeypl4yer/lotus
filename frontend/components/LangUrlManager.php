<?php
namespace frontend\components;

use Yii;
use \yii\web\UrlManager;

class LangUrlManager extends UrlManager
{
    public function init()
    {
        parent::init();
    }

    public function createUrl($params)
    {
        //Получаем сформированный URL(без префикса идентификатора языка)
        $url = parent::createUrl($params);

        //Добавляем к URL префикс - буквенный идентификатор языка
        return Yii::$app->params['prefix'][Yii::$app->session->get('lang', 'ru')] . $url;
    }
}