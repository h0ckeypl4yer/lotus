<?php
namespace frontend\components;

use Yii;
use yii\web\Request;
use common\models\Lang;

class LangRequest extends Request
{
    protected function resolveRequestUri()
    {
        $requestUri = parent::resolveRequestUri();
        $requestUriToList = explode('/', $requestUri);
        $lang_url = isset($requestUriToList[1]) ? $requestUriToList[1] : null;

        Yii::$app->session->remove('lang');

        if(in_array($lang_url, ['ru', 'en'])) {
            Yii::$app->session->set('lang', $lang_url);
            Yii::$app->language = 'ru-RU';
        }
        else
        {
            Yii::$app->session->set('lang', 'ru');
            Yii::$app->language = 'en-US';
        }

        $lang = Yii::$app->session->get('lang', 'ru');

        if( $lang_url !== null && $lang_url === $lang && strpos($requestUri, $lang) === 1 )
        {
            $requestUri = substr($requestUri, strlen($lang)+1 );
        }
        return $requestUri;
    }
}