<?php
/* @var $this yii\web\View */
/* @var $slides common\models\Slider[] */
$this->title = \Yii::t('app', 'Главная страница');
?>


<div class="home-slider-wrapper">
    <div class="home-slider">
        <?php foreach($slides as $slide) { ?>
            <?php $image_sizes = getimagesize(Yii::getAlias('@frontendweb') . $slide->getImageUrl()); ?>
            <?= $slide->link ? '<a href="'.$slide->link.'">' : '';?>
            <div class="home-slider-item image">
                <div class="home-slider-media">
                    <img src="<?= $slide->getImageUrl();?>" class="home-slider-media-element" data-width="<?= $image_sizes[0]; ?>"
                         data-height="<?= $image_sizes[1]; ?>" height="0"/>
                </div>
            </div>
            <?= $slide->link ? '</a>' : '';?>
        <?php } ?>
    </div>
    <div class="home-slider-white-line"></div>
</div>
<div class="home-page">
    <?php echo $this->render('include/_' . Yii::$app->params['file_prefix'][Yii::$app->session->get('lang', 'ru')] . 'index');?>
</div>
