<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = $name;
?>
<div class="site-error container">

    <h1>Страница не существует.</h1>

    <p>По вашему запросу старница не существует.</p>

</div>
