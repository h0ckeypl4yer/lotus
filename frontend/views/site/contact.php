<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

$this->title = 'Контакты';
?>

<?php echo $this->render('include/_' . Yii::$app->params['file_prefix'][Yii::$app->session->get('lang', 'ru')] . 'contact');?>


<a href="https://www.google.ru/maps/place/%D0%91%D0%BE%D0%BB%D1%8C%D1%88%D0%BE%D0%B9+%D0%A2%D0%BE%D0%BB%D0%BC%D0%B0%D1%87%D0%B5%D0%B2%D1%81%D0%BA%D0%B8%D0%B9+%D0%BF%D0%B5%D1%80.,+5+%D1%81%D1%82%D1%80%D0%BE%D0%B5%D0%BD%D0%B8%D0%B5+1">
    <div class="contacts-map" style="background-image: url(/images/map.jpg)"></div>
</a>

<div class="container">
    <div class="gl-text-block">
        <div class="contact-form-text"><?=Yii::t('app', 'Вы можете позвонить в удобное для вас время, написать письмо
            или задать вопрос через форму обратной связи — я вам обязательно отвечу');?></div>
        <div class="gl-form">
            <form>
                <div class="control-field">
                    <input class="custom-text" type="text" placeholder="<?=Yii::t('app', 'Ваше имя');?>"/>
                </div>
                <div class="control-field">
                    <input class="custom-text" type="text" placeholder="<?=Yii::t('app', 'Как с вами связаться');?>"/>
                </div>
                <div class="control-field">
                    <textarea class="custom-text" placeholder="<?=Yii::t('app', 'Ваш вопрос');?>"></textarea>
                </div>
                <div class="control-field">
                    <input class="custom-button" type="submit" value="<?=Yii::t('app', 'Спросить');?>"/>
                </div>
            </form>
        </div>
    </div>
</div>