<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
$this->title = Yii::t('main', 'Мы');
?>

<?php echo $this->render('include/_' . Yii::$app->params['file_prefix'][Yii::$app->session->get('lang', 'ru')] . 'we');?>

<?= \frontend\widgets\Reviews::widget(); ?>
