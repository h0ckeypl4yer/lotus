<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
$this->title = 'Эксклюзивный ювелирный дизайн';
?>

<?php echo $this->render('include/_' . Yii::$app->params['file_prefix'][Yii::$app->session->get('lang', 'ru')] . 'services');?>

<?= \frontend\widgets\Reviews::widget(); ?>