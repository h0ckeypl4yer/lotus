<div class="container">
    <div class="gl-text-block">
        <div class="gl-title">Lotus Jewellery House</div>
        <div class="gl-sub-title">Новая ювелирная философия</div>
        <div class="gl-text">
            <p>Ювелирная компания Lotus Jewellery House существует более десяти лет. Бренд появился в то время,
                когда ювелирный рынок был сформирован и такие понятия как высокое качество и эксклюзивность
                прочно заняли свои места в умах потребителей.

            <p>В этих условиях единственным критерием выживаемости на рынке ювелирных изделий могла стать
                уникальность бренда.
        </div>
        <a class="gl-readmore" href="#">Подробнее</a>
    </div>
</div>

<div class="parallax-block">
    <div class="parallax-block-in" style="background-image: url(/images/parallax.jpg);"></div>
    <div class="parallax-block-items">
        <div class="p-block-item" style="background-image: url(/images/p-item-1.jpg);"></div>
        <div class="p-block-item" style="background-image: url(/images/p-item-2.jpg);"></div>
        <div class="p-block-item" style="background-image: url(/images/p-item-3.jpg);"></div>
    </div>
</div>

<div class="container">
    <div class="gl-text-block">
        <div class="gl-lotus"></div>
        <div class="gl-title">Наши произведения искусства</div>
        <div class="gl-sub-title">Уникальная ювелирная коллекция</div>
        <div class="gl-text">
            <p>Наша задача, общаясь с заказчиком, понять, в чём именно состоит та волнующая идея, образ или
                символ, с которой к нам обращается наш клиент и доработать тот художественный образ, который бы
                наиболее точно отражал задумку. Используя обширный арсенал художественных средств, мы стремимся
                воплотить идею заказчика в материи ювелирного украшения, перевести ее из символического мира в
                мир материальный.
        </div>
        <a class="gl-readmore" href="#">Подробнее</a>
    </div>
</div>

<div class="photo-block" style="background-image: url(/images/photo-block.jpg);"></div>

<div class="container">
    <div class="gl-text-block gl-text-block-no-bottom">
        <div class="gl-lotus"></div>
        <div class="gl-title">Мы знаем всё о драгоненностях</div>
        <div class="gl-sub-title">Технологии изготовления, свойства камней, эзотерический смысл</div>
        <div class="gl-text">
            <div class="gl-form">
                <form>
                    <div class="control-field">
                        <input class="custom-text" type="text" placeholder="Ваше имя"/>
                    </div>
                    <div class="control-field">
                        <input class="custom-text" type="text" placeholder="Как с вами связаться"/>
                    </div>
                    <div class="control-field">
                        <textarea class="custom-text" placeholder="Ваш вопрос"></textarea>
                    </div>
                    <div class="control-field">
                        <input class="custom-button" type="submit" value="Спросить"/>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>