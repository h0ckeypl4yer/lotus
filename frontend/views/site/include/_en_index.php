<div class="container">
    <div class="gl-text-block">
        <div class="gl-title">Lotus Jewellery House</div>
        <div class="gl-sub-title">New jewelry philosophy</div>
        <div class="gl-text">
            <p> Jewelry Company Lotus Jewellery House has more than ten years. Brand appeared at the time, when the
                jewelry market was formed and concepts such as the high quality and exclusivity firmly taken its place
                in the minds of consumers.

            <p> In these circumstances, the only criterion for survival in the jewelry market could be the uniqueness of
                the brand.
        </div>
        <a class="gl-readmore" href="#">Readmore</a>
    </div>
</div>

<div class="parallax-block">
    <div class="parallax-block-in" style="background-image: url(/images/parallax.jpg);"></div>
    <div class="parallax-block-items">
        <div class="p-block-item" style="background-image: url(/images/p-item-1.jpg);"></div>
        <div class="p-block-item" style="background-image: url(/images/p-item-2.jpg);"></div>
        <div class="p-block-item" style="background-image: url(/images/p-item-3.jpg);"></div>
    </div>
</div>

<div class="container">
    <div class="gl-text-block">
        <div class="gl-lotus"></div>
        <div class="gl-title">Наши произведения искусства</div>
        <div class="gl-sub-title">Уникальная ювелирная коллекция</div>
        <div class="gl-text">
            <p>Our task, communicating with the customer, to understand what it is that exciting idea, image or
                                 symbols that apply to us and our client to refine the artistic image, which would
                                 most accurately reflects the idea. Using the vast arsenal of artistic means, we aim to
                                 embody the idea of the customer in the matter jewelry, transfer it from the symbolic
                world
                                 the material world.
        </div>
        <a class="gl-readmore" href="#">Readmore</a>
    </div>
</div>

<div class="photo-block" style="background-image: url(/images/photo-block.jpg);"></div>

<div class="container">
    <div class="gl-text-block gl-text-block-no-bottom">
        <div class="gl-lotus"></div>
        <div class="gl-title">Мы знаем всё о драгоненностях</div>
        <div class="gl-sub-title">Технологии изготовления, свойства камней, эзотерический смысл</div>
        <div class="gl-text">
            <div class="gl-form">
                <form>
                    <div class="control-field">
                        <input class="custom-text" type="text" placeholder="Ваше имя"/>
                    </div>
                    <div class="control-field">
                        <input class="custom-text" type="text" placeholder="Как с вами связаться"/>
                    </div>
                    <div class="control-field">
                        <textarea class="custom-text" placeholder="Ваш вопрос"></textarea>
                    </div>
                    <div class="control-field">
                        <input class="custom-button" type="submit" value="Спросить"/>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>