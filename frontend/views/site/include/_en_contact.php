<div class="container">
    <div class="gl-text-block">
        <div class="gl-title">Свяжитесь с нами</div>
        <div class="gl-sub-title">Контакты</div>
        <div class="gl-text">
            <p>Ювелирная компания Lotus Jewellery House существует более десяти лет. Бренд появился в то время,
                когда ювелирный рынок был сформирован и такие понятия как высокое качество и эксклюзивность прочно
                заняли свои места в умах потребителей. В этих условиях единственным критерием выживаемости на рынке
                ювелирных изделий могла стать уникальность бренда.</p>

            <div class="contacts-three-cols">
                <div class="contacts-col contacts-address">
                    <p>Офис расположен по адресу:

                    <p>Москва, Б. Толмачевский пер., д.5, стр. 1
                </div>
                <div class="contacts-col contacts-phone">
                    <p>8 (495) 943-41-85

                    <p>yuliashelest@lotusgem.ru
                </div>
                <div class="contacts-col contacts-fb">
                    facebook.com/lotusgem.ru
                </div>
            </div>
        </div>
    </div>
</div>