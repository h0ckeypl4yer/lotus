<?php
/* @var $this yii\web\View */
/* @var $categories common\models\BlogCategory[] */
$this->title = 'Блог';
?>
<div class="container">
    <div class="gl-text-block">
        <div class="gl-title"><?= Yii::t('app', 'Интересные истории'); ?></div>
        <div class="gl-sub-title"><?= Yii::t('app', 'Блог'); ?></div>
        <div class="gl-text">
            <p>
                <?= Yii::t('app',
                    'Ювелирная компания Lotus Jewellery House существует более десяти лет. Бренд появился в то время, когда ювелирный рынок был сформирован и такие понятия как высокое качество и эксклюзивность прочно заняли свои места в умах потребителей.'); ?>
        </div>
    </div>
    <div class="blog-tabs">
        <div class="blog-tabs-header">
            <?php foreach ($categories as $key => $category) {
                ?>
                <a href="#tab-<?= $key; ?>"<?= $key == 0 ? ' class="active"' : ''; ?>><?= $category->{Yii::$app->params['file_prefix'][Yii::$app->session->get('lang', 'ru')] . 'title'}; ?></a>
            <?php } ?>
        </div>
        <div class="blog-tabs-body">

            <?php foreach ($categories as $key => $category) {
                ?>
                <div class="blog-tabs-item<?= $key == 0 ? ' active' : ''; ?>" id="tab-<?= $key; ?>">
                    <div class="blog-list">
                        <div class="blog-list-grid"></div>
                        <?php foreach ($category->items as $i => $item) {
                            ?>

                            <div class="blog-list-item<?= $item->type == 1 ? ' blog-list-long' : ''; ?>">
                                <div class="blog-list-item-image"
                                     style="background-image: url(<?= $item->getImageUrl(); ?>);"></div>
                                <a href="<?= $item->getViewUrl(); ?>"
                                   class="blog-list-item-title"><?= $item->{Yii::$app->params['file_prefix'][Yii::$app->session->get('lang', 'ru')] . 'title'}; ?></a>

                                <div class="blog-list-item-date"><?= date('d.m.Y', $item->created_at); ?></div>
                                <div class="blog-list-item-text"><?= $item->{Yii::$app->params['file_prefix'][Yii::$app->session->get('lang', 'ru')] . 'intro'}; ?>
                                </div>
                            </div>
                            <?php if ($i == 0) {
                                ?>
                                <div class="blog-list-item blog-list-static">
                                    <div class="blog-list-item-image"
                                         style="background-image: url(<?= $category->getImageUrl(); ?>);"></div>
                                </div>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>