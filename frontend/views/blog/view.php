<?php
/* @var $this yii\web\View */
/* @var $model common\models\Blog */
$this->title = 'Блог - ' . $model->title;
?>

<div class="container">
    <div class="gl-text-block">
        <div class="gl-title"><?=Yii::t('app', 'Интересные историиx');?></div>
        <div class="gl-sub-title"><?= Yii::t('app', 'Изделия в деталях');?></div>
    </div>
    <div class="blog-item-blocks">
        <div class="blog-item-block">
            <div class="blog-item-text">
                <?= $model->{Yii::$app->params['file_prefix'][Yii::$app->session->get('lang', 'ru')] . 'text_first'}; ?>
            </div>
            <?php if (count($model->addImages))
            {
                ?>

                <div class="blog-item-image">
                    <div class="blog-item-slider-wrapper">
                        <div class="blog-item-slider">
                            <?php foreach ($model->addImages as $image)
                            {
                                ?>
                                <img src="<?= $image->getImageUrl(); ?>"/>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="blog-item-block">
            <?php if ($model->video)
            {
                ?>
                <div class="blog-item-image">
                    <?= $model->video; ?>
                </div>
            <?php } ?>
            <div class="blog-item-text">
                <?= $model->{Yii::$app->params['file_prefix'][Yii::$app->session->get('lang', 'ru')] . 'text_second'}; ?>
            </div>
        </div>
    </div>
    <div class="blog-item-navi">
        <div class="blog-item-navi-left">
            <?= $model->getPrev() ? \yii\helpers\Html::a(Yii::t('app', 'Предыдущая статья'), $model->getPrev()->getViewUrl()) : '';?>
        </div>
        <div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button"
             data-action="like" data-show-faces="false" data-share="false"></div>
        <div class="blog-item-navi-right">
            <?= $model->getNext() ? \yii\helpers\Html::a(Yii::t('app', 'Следующая статья'), $model->getNext()->getViewUrl()) : '';?>
        </div>
    </div>
</div>

<div id="fb-root"></div>
<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.0";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
