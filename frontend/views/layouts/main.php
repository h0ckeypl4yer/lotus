<?php
use yii\helpers\Html;
use frontend\assets\AppAsset;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<header id="header" class="header">
    <div class="container">

        <div class="header-lang-switcher">
            <?= \frontend\widgets\LangSwitcher::widget(); ?>
        </div>

        <a class="header-logo" href="<?=\yii\helpers\Url::to(['/']);?>"></a>

        <div class="header-line"></div>
        <div class="header-menu-wrap">
            <nav class="header-menu">
                <?= \frontend\widgets\MainMenu::widget(); ?>
            </nav>
        </div>
    </div>
</header>
<section id="content" class="content">
    <!-- START CONTENT-->
    <?= $content ?>
    <!-- END CONTENT-->
</section>

<?php echo $this->render('include/_footer');?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
