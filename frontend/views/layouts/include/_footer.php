<?php
/* @var $this \yii\web\View */
/* @var $content string */

?>

<footer id="footer" class="footer">
    <div class="decor-line"></div>
    <div class="container">
        <div class="footer-text">
            <p>Наш офис расположен по адресу:</p>

            <p>Москва, Б. Толмачевский пер., д.5, с. 1</p>
        </div>
        <a class="footer-logo" href="/"></a>
    </div>
</footer>