<?php
/* @var $this yii\web\View */
/* @var $items common\models\Portfolio[] */
?>

<div class="container">
    <div class="gl-text-block gl-text-block-no-bottom">
        <div class="gl-title">
            <?=Yii::t('app', 'Наши произведения искусства');?>
        </div>
        <div class="gl-sub-title"><?=Yii::t('app', 'Портфолио');?></div>
        <div class="gl-text">
        </div>
    </div>
</div>

<div class="portfolio-slider">
    <div class="swiper-container">
        <div class="swiper-wrapper">
            <?php
            $current_weight = 0;
            $max_weight = 6;

            foreach ($items as $item)
            {
                if(\common\models\Portfolio::$weights[$item->type] == $max_weight)
                {
                    if($current_weight > 0) {
                        echo '</div>';
                        $current_weight = 0;
                    }
                    ?>
                    <div class="swiper-slide<?=\common\models\Portfolio::$classes[$item->type];?>">
                        <a href="<?= $item->getViewUrl();?>" class="portfolio-item fancybox" rel="fancybox-group"
                           style="background-image: url(<?= $item->slider_image ? $item->getSliderImageUrl() : $item->getImageUrl(); ?>)"></a>
                    </div>
                <? }
                else {
                    if($current_weight == 0)
                    {
                        echo '<div class="swiper-slide">';
                    }

                    $current_weight += \common\models\Portfolio::$weights[$item->type];

                    if($current_weight > $max_weight)
                    {
                        echo '</div><div class="swiper-slide">';
                        $current_weight = \common\models\Portfolio::$weights[$item->type];
                    }

                    ?>

                    <div class="swiper-slide<?=\common\models\Portfolio::$classes[$item->type];?>">
                        <a href="<?= $item->getViewUrl();?>" class="portfolio-item fancybox" rel="fancybox-group"
                           style="background-image: url(<?= $item->getImageUrl(); ?>)"></a>
                    </div>

                    <?php
                }
                ?>

            <?php }
            if($current_weight > 0)
                echo '</div>';
            ?>

        </div>
    </div>
    <div class="swiper-navi">
        <a class="swiper-navi-left" href="#"></a>
        <a class="swiper-navi-right" href="#"></a>
    </div>
    <div class="swiper-scrollbar">
        <div class="swiper-scrollbar-in"></div>
    </div>
    <div class="swiper-white"></div>
</div>

<div class="container">
    <div class="portfolio-text">
        <?= Yii::t('app', 'Наша задача, общаясь с заказчиком, понять, в чём именно состоит та волнующая идея, образ или символ, с
        которой к нам обращается наш клиент и доработать тот художественный образ, который бы наиболее точно отражал
        задумку. Используя обширный арсенал художественных средств, мы стремимся воплотить идею заказчика в материи
        ювелирного украшения, перевести ее из символического мира в мир материальный.');?>
    </div>
</div>
