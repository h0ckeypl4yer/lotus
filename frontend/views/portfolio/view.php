<?php
/* @var $this yii\web\View */
/* @var $model common\models\Portfolio */
?>

<div class="portfolio-fly-page">
    <div class="portfolio-fly-page-image">
        <img src="<?= $model->getImageUrl();?>"/>
    </div>
    <div class="portfolio-fly-page-text">
        <p><?= $model->{Yii::$app->params['file_prefix'][Yii::$app->session->get('lang', 'ru')] . 'text'}; ?></p>
    </div>
    <?= $model->link ? \yii\helpers\Html::a(Yii::t('app', 'Узнать подробнее в блоге'), $model->link, ['class' => 'go-to-blog']) : ''; ?>
</div>
