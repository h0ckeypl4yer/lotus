<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%portfolio_image}}".
 *
 * @property integer $id
 * @property string $image
 * @property integer $portfolio_id
 */
class PortfolioImage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%portfolio_image}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['image', 'portfolio_id'], 'required'],
            [['portfolio_id'], 'integer'],
            [['image'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'           => 'ID',
            'image'        => 'Изображение',
            'portfolio_id' => 'Portfolio ID',
        ];
    }

    public function getImageUrl()
    {
        return Portfolio::UPLOAD_PATH . $this->image;
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete())
        {

            if (file_exists(Yii::getAlias('@frontendweb') . Portfolio::UPLOAD_PATH . $this->image))
            {
                unlink(Yii::getAlias('@frontendweb') . Portfolio::UPLOAD_PATH . $this->image);
            }

            return true;
        }
        return false;
    }
}
