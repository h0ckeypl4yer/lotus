<?php

namespace common\models;

use Yii;
use yii\helpers\Url;
use yii\web\UploadedFile;

/**
 * This is the model class for table "{{%portfolio}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $text
 * @property string $image
 * @property integer $ordering
 * @property integer $is_active
 * @property string $link
 * @property integer $type
 */
class Portfolio extends \yii\db\ActiveRecord
{
    public $images = [];
    public $old_images = [];

    const UPLOAD_PATH = '/uploads/portfolio/';

    public static $types = [
        1 => 'Узкий в полную высоту',
        2 => 'Узкий в 2/3 высоты',
        3 => 'Маленький',
        4 => 'Широкий во всю высоту'
    ];

    public static $weights = [
        1 => 6,
        2 => 4,
        3 => 1,
        4 => 6,
    ];

    public static $classes = [
        1 => '',
        2 => ' swiper-slide-medium',
        3 => ' swiper-slide-small',
        4 => ' swiper-slide-wide',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%portfolio}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ordering'], 'default', 'value' => 99],
            [['title', 'text', 'en_title', 'en_text', 'type'], 'required'],
            [['text', 'en_text'], 'string'],
            [['ordering', 'is_active', 'type'], 'integer'],
            [['title', 'en_title'], 'string', 'max' => 100],
            [['image'], 'file', 'extensions' => 'jpeg, jpg, png, gif', 'skipOnEmpty' => false, 'on' => 'create'],
            [['image'], 'file', 'extensions' => 'jpeg, jpg, png, gif', 'skipOnEmpty' => true, 'on' => 'update'],
            [['slider_image'], 'file', 'extensions' => 'jpeg, jpg, png, gif', 'skipOnEmpty' => true],
            [['old_images', 'images'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'           => 'ID',
            'title'        => 'Заголовок',
            'text'         => 'Текст',
            'en_title'     => 'Заголовок(англ)',
            'en_text'      => 'Текст(англ)',
            'image'        => 'Изображение',
            'slider_image' => 'Изображение для слайдера',
            'ordering'     => 'Порядок',
            'is_active'    => 'Активность',
            'link'         => 'Ссылка',
            'type'         => 'Тип отображения',
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {

        if (count($this->addImages)) {
            foreach ($this->addImages as $image) {
                if (!in_array($image->id, $this->old_images)) {
                    $image->delete();
                    unset($this->old_images[$image->id]);
                }
            }
        }

        if ($images = UploadedFile::getInstances($this, 'images')) {
            foreach ($images as $image) {
                $extension              = strtolower($image->getExtension());
                $addImage               = new PortfolioImage();
                $addImage->image        = Yii::$app->security->generateRandomString() . '.' . $extension;
                $addImage->portfolio_id = $this->id;
                $addImage->save();
                $image->saveAs(Yii::getAlias('@frontendweb') . self::UPLOAD_PATH . $addImage->image);
            }
        }

        parent::afterSave($insert, $changedAttributes);
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {

            if (count($this->addImages)) {
                foreach ($this->addImages as $image) {
                    $image->delete();
                }
            }
            return true;
        }
        return false;
    }

    public function getAddImages()
    {
        return $this->hasMany(PortfolioImage::className(), ['portfolio_id' => 'id']);
    }

    public function getImageUrl()
    {
        return self::UPLOAD_PATH . $this->image;
    }

    public function getSliderImageUrl()
    {
        return self::UPLOAD_PATH . $this->slider_image;
    }

    public function getIsActive()
    {
        return $this->is_active ? 'Да' : 'Нет';
    }

    public function getViewUrl()
    {
        return Url::toRoute(['portfolio/view', 'id' => $this->id]);
    }
}
