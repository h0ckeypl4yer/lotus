<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%review}}".
 *
 * @property integer $id
 * @property string $author
 * @property string $text
 * @property string $image
 * @property integer $ordering
 * @property integer $is_active
 */
class Review extends \yii\db\ActiveRecord
{

    const UPLOAD_PATH = '/uploads/review/';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%review}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ordering'], 'default', 'value' => 99],
            [['author', 'en_author', 'text', 'en_text'], 'required'],
            [['text', 'en_text'], 'string'],
            [['ordering'], 'integer'],
            [['is_active'], 'boolean'],
            [['author', 'en_author'], 'string', 'max' => 50],
            [['image'], 'file', 'extensions' => 'jpeg, jpg, png, gif', 'skipOnEmpty' => false, 'on' => 'create'],
            [['image'], 'file', 'extensions' => 'jpeg, jpg, png, gif', 'skipOnEmpty' => true, 'on' => 'update'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'        => 'ID',
            'author'    => 'Автор',
            'text'      => 'Текст',
            'en_author' => 'Автор(англ)',
            'en_text'   => 'Текст(англ)',
            'image'     => 'Изображение',
            'ordering'  => 'Порядок',
            'is_active' => 'Активность',
        ];
    }

    public function getImageUrl()
    {
        return self::UPLOAD_PATH . $this->image;
    }

    public function getIsActive()
    {
        return $this->is_active ? 'Да' : 'Нет';
    }
}
