<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%blog_category}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $image
 * @property integer $ordering
 */
class BlogCategory extends \yii\db\ActiveRecord
{

    const UPLOAD_PATH = '/uploads/blog/';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%blog_category}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ordering'], 'default', 'value' => 99],
            [['title', 'en_title', 'ordering'], 'required'],
            [['ordering'], 'integer'],
            [['title', 'en_title'], 'string', 'max' => 30],
            [['image'], 'file', 'extensions' => 'jpeg, jpg, png, gif', 'skipOnEmpty' => false, 'on' => 'create'],
            [['image'], 'file', 'extensions' => 'jpeg, jpg, png, gif', 'skipOnEmpty' => true, 'on' => 'update'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'       => 'ID',
            'title'    => 'Заголовок',
            'en_title' => 'Заголовок(англ)',
            'image'    => 'Изображение',
            'ordering' => 'Порядок',
        ];
    }

    public function getImageUrl()
    {
        return self::UPLOAD_PATH . $this->image;
    }

    public function getTitle()
    {
        if(Yii::$app->session->get('lang', 'ru') == 'en')
            return $this->en_title;
        else
            return $this->title;
    }

    public function getItems()
    {
        return $this->hasMany(Blog::className(), ['category_id' => 'id']);
    }
}
