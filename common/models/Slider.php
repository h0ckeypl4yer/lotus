<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%slider}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $link
 * @property string $image
 * @property integer $ordering
 * @property integer $is_active
 */
class Slider extends \yii\db\ActiveRecord
{

    const UPLOAD_PATH = '/uploads/slider/';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%slider}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ordering'], 'default', 'value' => 99],
            [['title'], 'required'],
            [['ordering'], 'integer'],
            [['is_active'], 'boolean'],
            [['title'], 'string', 'max' => 32],
            [['link'], 'string', 'max' => 255],
            [['image'], 'file', 'extensions' => 'jpeg, jpg, png, gif', 'skipOnEmpty' => false, 'on' => 'create'],
            [['image'], 'file', 'extensions' => 'jpeg, jpg, png, gif', 'skipOnEmpty' => true, 'on' => 'update'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'link' => 'Ссылка',
            'image' => 'Изображение',
            'ordering' => 'Порядок',
            'is_active' => 'Для англ версии',
        ];
    }

    public function getImageUrl()
    {
        return self::UPLOAD_PATH . $this->image;
    }

    public function getIsActive()
    {
        return $this->is_active ? 'Да' : 'Нет';
    }
}
