<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%blog_image}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $image
 * @property integer $ordering
 */
class BlogImage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%blog_image}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['image'], 'required'],
            [['image'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image' => 'Изображение',
        ];
    }

    public function getImageUrl()
    {
        return Blog::UPLOAD_PATH . $this->image;
    }
}
