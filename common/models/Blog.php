<?php

namespace common\models;

use Yii;
use yii\helpers\Url;
use yii\web\UploadedFile;

/**
 * This is the model class for table "{{%blog}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $text_first
 * @property string $text_second
 * @property string $intro
 * @property integer $created_at
 * @property string $image
 * @property integer $ordering
 * @property integer $type
 * @property string $video
 */
class Blog extends \yii\db\ActiveRecord
{
    public $images = [];
    public $old_images = [];

    const UPLOAD_PATH = '/uploads/blog/';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%blog}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ordering'], 'default', 'value' => 99],
            [
                [
                    'title',
                    'en_title',
                    'text_first',
                    'text_second',
                    'en_text_first',
                    'en_text_second',
                    'intro',
                    'created_at',
                    'type',
                    'category_id'
                ],
                'required'
            ],
            [['text_first', 'text_second', 'en_text_first', 'en_text_second', 'video'], 'string'],
            [['ordering', 'type'], 'integer'],
            [['title', 'en_title'], 'string', 'max' => 30],
            [['intro', 'en_intro'], 'string', 'max' => 255],
            ['created_at', 'date', 'format' => 'dd.MM.yyyy'],
            [['image'], 'file', 'extensions' => 'jpeg, jpg, png, gif', 'skipOnEmpty' => false, 'on' => 'create'],
            [['image'], 'file', 'extensions' => 'jpeg, jpg, png, gif', 'skipOnEmpty' => true, 'on' => 'update'],
            [['old_images', 'images'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'             => 'ID',
            'title'          => 'Заголовок',
            'text_first'     => 'Текст 1',
            'text_second'    => 'Текст 2',
            'intro'          => 'Анонс',
            'en_title'       => 'Заголовок(англ)',
            'en_text_first'  => 'Текст 1(англ)',
            'en_text_second' => 'Текст 2(англ)',
            'en_intro'       => 'Анонс(англ)',
            'created_at'     => 'Дата публикации',
            'image'          => 'Изображение',
            'ordering'       => 'Порядок',
            'type'           => 'Большая картинка в списке',
            'video'          => 'Код видео',
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->created_at = strtotime($this->created_at);
            return true;
        }
        return false;
    }

    public function afterSave($insert, $changedAttributes)
    {

        if (count($this->addImages)) {
            foreach ($this->addImages as $image) {
                if (!in_array($image->id, $this->old_images)) {
                    $image->delete();
                    unset($this->old_images[$image->id]);
                }
            }
        }

        if ($images = UploadedFile::getInstances($this, 'images')) {
            foreach ($images as $image) {
                $extension         = strtolower($image->getExtension());
                $addImage          = new BlogImage();
                $addImage->image   = Yii::$app->security->generateRandomString() . '.' . $extension;
                $addImage->blog_id = $this->id;
                $addImage->save();
                $image->saveAs(Yii::getAlias('@frontendweb') . self::UPLOAD_PATH . $addImage->image);
            }
        }

        parent::afterSave($insert, $changedAttributes);
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {

            if (count($this->addImages)) {
                foreach ($this->addImages as $image) {
                    $image->delete();
                }
            }
            return true;
        }
        return false;
    }

    public function getAddImages()
    {
        return $this->hasMany(BlogImage::className(), ['blog_id' => 'id']);
    }

    public function getImageUrl()
    {
        return self::UPLOAD_PATH . $this->image;
    }

    public function getIsActive()
    {
        return $this->is_active ? 'Да' : 'Нет';
    }

    public function getViewUrl()
    {
        return Url::toRoute(['blog/view', 'id' => $this->id]);
    }

    public function getCategory()
    {
        return $this->hasOne(BlogCategory::className(), ['id' => 'category_id']);
    }


    public function getPrev()
    {
        $modelPrev = self::find()->where('category_id = :category_id AND (ordering < :ordering OR (ordering = :ordering AND id < :id))',
        [
            ':id'          => $this->id,
            ':ordering'    => $this->ordering,
            ':category_id' => $this->category_id,
        ])->one();
        return $modelPrev;
    }

    public function getNext()
    {
        $modelPrev = self::find()->where('category_id = :category_id AND (ordering > :ordering OR (ordering = :ordering AND id > :id))',
        [
            ':id'          => $this->id,
            ':ordering'    => $this->ordering,
            ':category_id' => $this->category_id,
        ])->one();

        return $modelPrev;
    }
}
