<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\BlogCategory */

$this->title = 'Изменить категорию';
$this->params['breadcrumbs'][] = ['label' => 'Категории блога', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="blog-category-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
