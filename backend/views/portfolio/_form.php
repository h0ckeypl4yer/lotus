<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Portfolio */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="portfolio-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li class="active"><a href="#tab-info" role="tab" data-toggle="tab">Основная информация</a></li>
        <!--<li><a href="#tab-images" role="tab" data-toggle="tab">Доп. изображения</a></li>-->
    </ul>


    <!-- Tab panes -->
    <div class="tab-content">
        <div class="tab-pane active" id="tab-info">
            <p>
            <?= $form->field($model, 'title')->textInput(['maxlength' => 100]) ?>
            <?= $form->field($model, 'en_title')->textInput(['maxlength' => 100]) ?>

            <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>
            <?= $form->field($model, 'en_text')->textarea(['rows' => 6]) ?>

            <?= $model->image ? Html::img($model->getImageUrl(), ['style' => 'width:300px']) : ''; ?>

            <?= $form->field($model, 'image')->fileInput() ?>

            <?= $model->slider_image ? Html::img($model->getSliderImageUrl(), ['style' => 'width:300px']) : ''; ?>

            <?= $form->field($model, 'slider_image')->fileInput() ?>

            <?= $form->field($model, 'ordering')->textInput() ?>

            <?= $form->field($model, 'is_active')->checkbox(); ?>

            <?= $form->field($model, 'link')->textInput(['maxlength' => 255]) ?>

            <?= $form->field($model, 'type')->dropDownList(\common\models\Portfolio::$types) ?>
            </p>
        </div>

        <!--<div class="tab-pane" id="tab-images">
            <p>
            <div class="row">
                <?php foreach($model->addImages as $key=>$item) { ?>
                    <div class="col-sm-6 col-md-4">
                        <div class="thumbnail">
                            <img src="<?=$item->getImageUrl();?>" alt="" style="height: 150px;">
                        </div>
                        <div class="caption">
                            <?= Html::activeHiddenInput($model, 'old_images[]', ['value' => $item->id]); ?>
                            <a href="#" onclick="$(this).closest('.col-sm-6').remove();">Удалить</a>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <br>

            <div class="tab-images-in row">

                <div class="tab-images-item tab-images-hidden col-sm-12" style="display: none">
                    <div class="form-group">
                        <div class="col-sm-3"></div>
                        <div class="col-sm-3">
                            <?= Html::activeFileInput($model, 'images[]'); ?>
                            <br>
                        </div>
                        <div class="col-sm-3">
                            <a href="#" onclick="$(this).closest('.tab-images-item').remove();">Удалить</a>
                        </div>
                    </div>
                </div>

            </div>

            <div class="form-group">
                <div class="col-sm-3"></div>
                <div class="col-sm-9">

                    <a href="#" onclick="$('.tab-images-in').append($('.tab-images-hidden').clone().show().removeClass('tab-images-hidden'));">Добавить</a>

                </div>
            </div>

            </p>
        </div>-->
    </div>



    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
