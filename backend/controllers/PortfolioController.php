<?php

namespace backend\controllers;

use Yii;
use common\models\Portfolio;
use backend\models\PortfolioSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * PortfolioController implements the CRUD actions for Portfolio model.
 */
class PortfolioController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Portfolio models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PortfolioSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Portfolio model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model           = new Portfolio();
        $model->scenario = 'create';
        $model->is_active = 1;

        if ($model->load(Yii::$app->request->post()))
        {
            $filename = '';

            if ($model->image = UploadedFile::getInstance($model, 'image'))
            {
                $extension = strtolower($model->image->getExtension());
                $filename  = Yii::$app->security->generateRandomString() . '.' . $extension;
            }

            $slider_filename = '';

            if ($model->slider_image = UploadedFile::getInstance($model, 'slider_image'))
            {
                $extension = strtolower($model->slider_image->getExtension());
                $slider_filename  = Yii::$app->security->generateRandomString() . '.' . $extension;
            }

            if ($model->validate())
            {
                $model->image->saveAs(Yii::getAlias('@frontendweb') . Portfolio::UPLOAD_PATH . $filename);
                $model->image = $filename;

                if($model->slider_image)
                {
                    $model->slider_image->saveAs(Yii::getAlias('@frontendweb') . Portfolio::UPLOAD_PATH . $slider_filename);
                    $model->slider_image = $slider_filename;
                }

                $model->save(false);
                return $this->redirect('index');
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Portfolio model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model           = $this->findModel($id);
        $model->scenario = 'update';

        $old_filename = $model->image;

        $old_slider_filename = $model->slider_image;


        if ($model->load(Yii::$app->request->post()))
        {
            $filename = '';

            if ($model->image = UploadedFile::getInstance($model, 'image'))
            {
                $extension = strtolower($model->image->getExtension());
                $filename  = Yii::$app->security->generateRandomString() . '.' . $extension;
            }

            $slider_filename = '';

            if ($model->slider_image = UploadedFile::getInstance($model, 'slider_image'))
            {
                $extension = strtolower($model->slider_image->getExtension());
                $slider_filename  = Yii::$app->security->generateRandomString() . '.' . $extension;
            }


            if ($model->validate())
            {
                if ($model->image)
                {
                    // Удаляем старый файл
                    if ($old_filename && file_exists(Portfolio::UPLOAD_PATH . $old_filename))
                    {
                        unlink(Yii::getAlias('@frontend') . Portfolio::UPLOAD_PATH . $old_filename);
                    }

                    $model->image->saveAs(Yii::getAlias('@frontendweb') . Portfolio::UPLOAD_PATH . $filename);
                    $model->image = $filename;
                }
                else
                {
                    $model->image = $old_filename;
                }

                if($model->slider_image)
                {
                    // Удаляем старый файл
                    if ($old_slider_filename && file_exists(Portfolio::UPLOAD_PATH . $old_slider_filename))
                    {
                        unlink(Yii::getAlias('@frontend') . Portfolio::UPLOAD_PATH . $old_slider_filename);
                    }

                    $model->slider_image->saveAs(Yii::getAlias('@frontendweb') . Portfolio::UPLOAD_PATH . $slider_filename);
                    $model->slider_image = $slider_filename;
                }
                else
                {
                    $model->slider_image = $old_slider_filename;
                }


                $model->save(false);
                return $this->redirect(['index']);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Portfolio model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Portfolio model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Portfolio the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Portfolio::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
