<?php

namespace backend\controllers;

use Yii;
use common\models\Slider;
use backend\models\SliderSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * SliderController implements the CRUD actions for Slider model.
 */
class SliderController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Slider models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel  = new SliderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Slider model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model           = new Slider();
        $model->scenario = 'create';
        $model->is_active = 1;

        if ($model->load(Yii::$app->request->post()))
        {
            $filename = '';

            if ($model->image = UploadedFile::getInstance($model, 'image'))
            {
                $extension = strtolower($model->image->getExtension());
                $filename  = Yii::$app->security->generateRandomString() . '.' . $extension;
            }

            if ($model->validate())
            {
                $model->image->saveAs(Yii::getAlias('@frontendweb') . Slider::UPLOAD_PATH . $filename);
                $model->image = $filename;
                $model->save(false);
                return $this->redirect(['index']);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Slider model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model           = $this->findModel($id);
        $model->scenario = 'update';

        $old_filename = $model->image;


        if ($model->load(Yii::$app->request->post()))
        {
            $filename = '';

            if ($model->image = UploadedFile::getInstance($model, 'image'))
            {
                $extension = strtolower($model->image->getExtension());
                $filename  = Yii::$app->security->generateRandomString() . '.' . $extension;
            }


            if ($model->validate())
            {
                if ($model->image)
                {
                    // Удаляем старый файл
                    if ($old_filename && file_exists(Slider::UPLOAD_PATH . $old_filename))
                    {
                        unlink(Yii::getAlias('@frontend') . Slider::UPLOAD_PATH . $old_filename);
                    }

                    $model->image->saveAs(Yii::getAlias('@frontendweb') . Slider::UPLOAD_PATH . $filename);
                    $model->image = $filename;
                }
                else
                {
                    $model->image = $old_filename;
                }
                $model->save(false);
                return $this->redirect(['index']);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Slider model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Slider model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Slider the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Slider::findOne($id)) !== null)
        {
            return $model;
        }
        else
        {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
