<?php

namespace backend\controllers;

use Yii;
use common\models\Review;
use backend\models\ReviewSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ReviewController implements the CRUD actions for Review model.
 */
class ReviewController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Review models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ReviewSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Review model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model           = new Review();
        $model->scenario = 'create';
        $model->is_active = 1;

        if ($model->load(Yii::$app->request->post()))
        {
            $filename = '';

            if ($model->image = UploadedFile::getInstance($model, 'image'))
            {
                $extension = strtolower($model->image->getExtension());
                $filename  = Yii::$app->security->generateRandomString() . '.' . $extension;
            }

            if ($model->validate())
            {
                $model->image->saveAs(Yii::getAlias('@frontendweb') . Review::UPLOAD_PATH . $filename);
                $model->image = $filename;
                $model->save(false);
                return $this->redirect(['index']);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Review model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model           = $this->findModel($id);
        $model->scenario = 'update';

        $old_filename = $model->image;


        if ($model->load(Yii::$app->request->post()))
        {
            $filename = '';

            if ($model->image = UploadedFile::getInstance($model, 'image'))
            {
                $extension = strtolower($model->image->getExtension());
                $filename  = Yii::$app->security->generateRandomString() . '.' . $extension;
            }


            if ($model->validate())
            {
                if ($model->image)
                {
                    // Удаляем старый файл
                    if ($old_filename && file_exists(Review::UPLOAD_PATH . $old_filename))
                    {
                        unlink(Yii::getAlias('@frontend') . Review::UPLOAD_PATH . $old_filename);
                    }

                    $model->image->saveAs(Yii::getAlias('@frontendweb') . Review::UPLOAD_PATH . $filename);
                    $model->image = $filename;
                }
                else
                {
                    $model->image = $old_filename;
                }
                $model->save(false);
                return $this->redirect(['index']);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Review model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Review model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Review the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Review::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
